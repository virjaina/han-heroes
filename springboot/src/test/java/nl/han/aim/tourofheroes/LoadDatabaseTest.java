package nl.han.aim.tourofheroes;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class LoadDatabaseTest {
    @Mock HeroRepository heroRepository;

    @Test
    public void loadCallsRepositorySixTimes() throws Exception {
        LoadDatabase loadDatabase = new LoadDatabase();
        loadDatabase.initDatabase(heroRepository).run();
        verify(heroRepository).deleteAll();
        verify(heroRepository, times(6)).save(any(Hero.class));
    }

}
